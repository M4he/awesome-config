-----------------------------------------------------------------------------------------------------------------------
--                                              Mac'ish titlebars                                                    --
-----------------------------------------------------------------------------------------------------------------------

-- Grab environment
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local gears = require("gears")
local textbox = require("wibox.widget.textbox")

local redutil = require("redflat.util")
local helpers = require("redflat-extra.helpers")

local PI    = 2*math.asin(1)
local rad_N = 3*PI/2 -- north in radians
local rad_E = 0      -- east in radians
local rad_S = PI/2   -- south in radians
local rad_W = PI     -- west in radians

local is_maximized = helpers.client_is_maximized

-- Initialize tables and vars for module
-----------------------------------------------------------------------------------------------------------------------
local titlebar = {}

-- Style constants with optional overrides by theme
-----------------------------------------------------------------------------------------------------------------------
local style = {
	top_height       = 27, -- top titlebar height
	bottom_height    = 8,  -- bottom titlebar height
	corner_radius    = 6,  -- roundness of window corners
	button_size      = { w = 15, h = 15 }, -- titlebar button size
	button_padding   = 8,  -- padding between titlebar buttons
	button_shape     = gears.shape.circle,
	caption_enabled  = true,
	caption_centered = true,
	caption_margin   = 4,  -- only applies if 'caption_centered' is 'false'

	-- window matching rules for windows that should receive a dark titlebar
	dark_mode_rules  = {
		{ class = "XTerm" },
	},

	-- Setting the following to true omits any bottom and side border
	-- (the fake shadows) and removes the outer line (fake shadow) from
	-- the top border as well as not drawing any rounded corners.
	-- This is meant to be used with compositor settings that render
	-- shadows and rounded corners themselves, minimizing the effort
	-- required by awesome.
	compositor_draws_shadows_and_corners  = true,
	----------------------------------------------------------
	-- base colors
	colors = {
		titlebar = {
			active = {
				glossline      = "#f1f2f2",
				gradient_start = "#e0e0df",
				gradient_stop  = "#c9cacb",
				title          = "#333333",
			},
			inactive = {
				glossline      = "#f1f2f2",
				gradient_start = "#e0e0df",
				gradient_stop  = "#c9cacb",
				title          = "#888888",
			},
			inner_shade        = "#9f9f9f", -- line between client and titlebar decoration
		},
		dark_titlebar = {
			active = {
				glossline      = "#424242",
				gradient_start = "#383838",
				gradient_stop  = "#292929",
				title          = "#FFFFFF",
			},
			inactive = {
				glossline      = "#505050",
				gradient_start = "#454545",
				gradient_stop  = "#363636",
				title          = "#CCCCCC",
			},
			inner_shade        = "#323232", -- line between client and titlebar decoration
		},
		buttons = {
			close = { --- close button
				base             = "#fc5753",
				highlight_factor = 0.25,
				border_factor    = -0.2,
			},
			min = { --- minimize button
				base             = "#fdbc40",
				highlight_factor = 0.08,
				border_factor    = -0.15,
			},
			max = { --- maximize button
				base             = "#33c748",
				highlight_factor = 0.08,
				border_factor    = -0.15,
			},
			off = { --- unfocused button
				base             = "#BFBFBF",
				highlight_factor = 0.08,
				border_factor    = -0.1,
			},
		},
		dark_buttons = {
			close = { --- close button
				base             = "#fc5753",
				highlight_factor = 0.25,
				border_factor    = -0.175,
			},
			min = { --- minimize button
				base             = "#fdbc40",
				highlight_factor = 0.08,
				border_factor    = -0.2,
			},
			max = { --- maximize button
				base             = "#33c748",
				highlight_factor = 0.08,
				border_factor    = -0.2,
			},
			off = { --- unfocused button
				base             = "#BFBFBF",
				highlight_factor = 0.08,
				border_factor    = -0.25,
			},
		}
	},

	-- lines, shadows, borders
	color_outer_shade         = "#00000044", -- fake shadow line around client and titlebars
	color_outer_shade_solid   = "#555555",   -- color_outer_shade for maximized windows
	-- titlebar caption colors
	color_title_active        = "#333333",
	color_title_inactive      = "#888888",
}

-- merge any overrides from the beautiful theme definitions
style = redutil.table.merge(style, redutil.table.check(beautiful, "titlebars.macish") or {})

-- calculate all button colors
for _, mode in ipairs({ '', 'dark_' }) do
	for _, t in ipairs({ 'close', 'min', 'max', 'off' }) do
		local base_color = style.colors[mode .. 'buttons'][t].base
		local highlight_factor = style.colors[mode .. 'buttons'][t].highlight_factor
		local border_factor = style.colors[mode .. 'buttons'][t].border_factor

		style[ mode .. t .. "_button_normal_color" ] = gears.color(base_color)
		style[ mode .. t .. "_button_border_color" ] = gears.color(helpers.scale_hex_color(base_color, 1 + border_factor ))
		style[ mode .. t .. "_button_hover_color" ]  = gears.color(helpers.scale_hex_color(base_color, 1 + highlight_factor ))
	end
end

local function generate_gradient(colors)
	local gradient_stops
	if style.compositor_draws_shadows_and_corners then
		gradient_stops = {
			-- the first stop is the highlight gloss line which is less than 2 pixels in height
			{ 0.5/style.top_height, colors.glossline },
			{ 1/style.top_height, colors.gradient_start },
			{ 1, colors.gradient_stop }
		}
	else
		gradient_stops = {
			-- the first stop is the highlight gloss line which is less than 2 pixels in height
			-- if we have a fake 1px outer shadow we need to compensate for that
			{ 1.5/style.top_height, colors.glossline },
			{ 2/style.top_height, colors.gradient_start },
			{ 1, colors.gradient_stop }
		}
	end
	return gears.color.create_pattern(gears.color.create_pattern({
		type = "linear",
		from = {0, 0},
		to = {0, style.top_height},
		stops = gradient_stops
	}))
end

style.gradient_active = generate_gradient(style.colors.titlebar.active)
style.gradient_inactive = generate_gradient(style.colors.titlebar.inactive)
style.gradient_dark_active = generate_gradient(style.colors.dark_titlebar.active)
style.gradient_dark_inactive = generate_gradient(style.colors.dark_titlebar.inactive)

local tbar_button_layout_y_offset = style.top_height/2 - style.button_size.h/2

---------------------------------------------------------------------------------------------------

-- mouse button mapping for the whole titlebar
local function default_title_actions(c)
	return awful.button(
		{ }, 1,
		function()
			if c.focusable then client.focus = c end; c:raise()
			awful.mouse.client.move(c)
		end
	)
end

-- 'compositor_draws_shadows_and_corners = false' version of top titlebar
local function make_top(c)
	return function(widget, context, cr, width, height)

		local active = client.focus == c
		local dark_mode = c.valid and c._dark_titlebars
		local maximized = is_maximized(c)
		local corner_radius = maximized and 0 or style.corner_radius
		local color_outer_shade = gears.color(maximized and style.color_outer_shade_solid or style.color_outer_shade)
		local color_inner_shade = gears.color(
			dark_mode and style.colors.dark_titlebar.inner_shade or style.colors.titlebar.inner_shade
		)

		-- fake outer shadow
		cr:set_source(color_outer_shade)
		if maximized then
			cr:rectangle(0, 0, width, height)
		else
			cr:move_to(0, height)
			cr:line_to(0, corner_radius)
			cr:arc(corner_radius, corner_radius, corner_radius, rad_W, rad_N)
			cr:line_to(width - corner_radius, 0)
			cr:arc(width - corner_radius, corner_radius, corner_radius, rad_N, rad_E)
			cr:line_to(width, height)
		end
		cr:fill()

		local side_border_size = maximized and 0 or 1

		-- main titlebar surface
		if dark_mode then
			cr:set_source(active and style.gradient_dark_active or style.gradient_dark_inactive)
		else
			cr:set_source(active and style.gradient_active or style.gradient_inactive)
		end
		if maximized then
			cr:rectangle(0, 1, width, height-1)
		else
			cr:move_to(1, height)
			cr:line_to(1, corner_radius+1)
			cr:arc(corner_radius, corner_radius, corner_radius-1, rad_W, rad_N)
			cr:line_to(width - corner_radius - 1, 1)
			cr:arc(width - corner_radius, corner_radius, corner_radius-1, rad_N, rad_E)
			cr:line_to(width-1, height)
		end
		cr:fill()

		-- bottom content divider
		cr:set_source(color_inner_shade)
		cr:rectangle(side_border_size, height-1, width-2*side_border_size, 1)
		cr:fill()
	end
end

-- the compositor_draws_shadows_and_corners version of the top titlebar
local function make_top_minimal(c)
	return function(widget, context, cr, width, height)

		local active = client.focus == c
		local dark_mode = c.valid and c._dark_titlebars
		local color_inner_shade = gears.color(
			dark_mode and style.colors.dark_titlebar.inner_shade or style.colors.titlebar.inner_shade
		)

		-- main titlebar surface
		if dark_mode then
			cr:set_source(active and style.gradient_dark_active or style.gradient_dark_inactive)
		else
			cr:set_source(active and style.gradient_active or style.gradient_inactive)
		end
		cr:rectangle(0, 0, width, height)
		cr:fill()

		-- bottom content divider
		cr:set_source(color_inner_shade)
		cr:rectangle(0, height-1, width, 1)
		cr:fill()
	end
end

-- left fake shadow line, only for 'compositor_draws_shadows_and_corners = false'
local function make_left(c)
	return function(widget, context, cr, width, height)
		local maximized = is_maximized(c)
		local color_outer_shade = gears.color(maximized and style.color_outer_shade_solid or style.color_outer_shade)
		-- fake outer shadow
		cr:set_source(color_outer_shade)
		cr:rectangle(0, 0, width, height)
		cr:fill()
	end
end

-- right fake shadow line, only for 'compositor_draws_shadows_and_corners = false'
local function make_right(c)
	return function(widget, context, cr, width, height)
		local maximized = is_maximized(c)
		local color_outer_shade = gears.color(maximized and style.color_outer_shade_solid or style.color_outer_shade)
		-- fake outer shadow
		cr:set_source(color_outer_shade)
		cr:rectangle(0, 0, width, height)
		cr:fill()
	end
end

-- bottom rounded part, only for 'compositor_draws_shadows_and_corners = false'
local function make_bottom(c)
	return function(widget, context, cr, width, height)
		local active = client.focus == c
		local maximized = is_maximized(c)
		local corner_radius = maximized and 0 or style.corner_radius
		local color_outer_shade = gears.color(maximized and style.color_outer_shade_solid or style.color_outer_shade)
		local color_inner_shade = gears.color(style.color_inner_shade)

		cr:set_source(color_outer_shade)
		if maximized then
			cr:rectangle(0, 0, width, height)
		else
			cr:move_to(width, 0)
			cr:line_to(width, height - corner_radius)
			cr:arc(width - corner_radius, height - corner_radius, corner_radius, rad_E, rad_S)
			cr:line_to(corner_radius, height)
			cr:arc(corner_radius, height - corner_radius, corner_radius, rad_S, rad_W)
			cr:line_to(0, 0)
		end
		cr:fill()

		local side_border_size = maximized and 0 or 1

		cr:set_source(active and style.color_active or style.color_inactive)
		if maximized then
			cr:rectangle(0, 0, width, height-1)
		else
			cr:move_to(width-1, 0)
			cr:line_to(width-1, height - corner_radius-1)
			cr:arc(width - corner_radius, height - corner_radius, corner_radius-1, rad_E, rad_S)
			cr:line_to(corner_radius+1, height-1)
			cr:arc(corner_radius, height - corner_radius, corner_radius-1, rad_S, rad_W)
			cr:line_to(1, 0)
		end
		cr:fill()

		-- top content divider
		cr:set_source(color_inner_shade)
		cr:rectangle(side_border_size, 0, width-2*side_border_size, 1)
		cr:fill()
	end
end

local function make_title_caption(c)
	local ret = textbox()
	ret:set_font(beautiful.fonts.titlebar or "Sans 10")
	ret:set_align(style.caption_centered and "center" or "left")
	local dark_mode = c.valid and c._dark_titlebars
	local function update()
		local name = awful.util.escape(c.name or "")
		local focus = client.focus == c
		local color_pool = dark_mode and style.colors.dark_titlebar or style.colors.titlebar
		local text_color = focus and color_pool.active.title or color_pool.inactive.title
		ret:set_markup("<span foreground='" .. text_color .. "'> " .. name .. " </span>")
	end
	c:connect_signal("property::name", update)
	c:connect_signal("focus", update)
	c:connect_signal("unfocus", update)
	update()
	return ret
end

local function make_button_wrapper(c)
	local ret = wibox.layout.manual()
	ret.forced_width = style.button_size.w + style.button_padding
	ret.forced_height = style.top_height
	return ret
end

local function make_button(c, button_type)
	local ret = wibox.widget.base.make_widget()
	local dark_mode = c.valid and c._dark_titlebars
	ret.state = ""
	ret.draw = function(widget, context, cr, width, height)
		local state = (ret.state == "hover") and "hover" or "normal"
		local btype = (client.focus == c) and button_type or "off"
		if dark_mode then btype = "dark_" .. btype end
		local inner_color  = style[ btype .. "_button_" .. state .. "_color" ]
		local border_color = style[ btype .. "_button_border_color" ]
		gears.shape.transform(style.button_shape)
		:translate(.5,.5)(cr, style.button_size.w-1, style.button_size.h-1)
		cr:set_source(inner_color)
		cr:fill()
		cr:set_line_width(1)
		gears.shape.transform(style.button_shape)
		:translate(.5,.5)(cr, style.button_size.w-1, style.button_size.h-1)
		cr:set_source(border_color)
		cr:stroke()
	end

	ret.set_hover = function(widget, is_hover)
		ret.state = is_hover and "hover" or ""
		ret:emit_signal("widget::redraw_needed")
	end

	ret.fit = function(parent, context, widget, width, height)
		return ret.forced_width, ret.forced_height
	end

	ret:connect_signal("mouse::enter", function()
		ret:set_hover(true)
	end)
	ret:connect_signal("mouse::leave", function()
		ret:set_hover()
	end)

	ret.forced_width = style.button_size.w
	ret.forced_height = style.button_size.h

	return ret
end

local function make_close_button(c)
	local btn = make_button(c, "close")
	btn:buttons(awful.button({ }, 1, nil, function()
		c:kill()
	end))
	local ret = make_button_wrapper(c)
	ret:add_at(btn, {x=0, y=tbar_button_layout_y_offset})
	return ret
end

local function make_maximize_button(c)
	local btn = make_button(c, "max")
	btn:buttons(awful.button({ }, 1, nil, function()
		c.maximized = not c.maximized
		btn.state = ""
		btn:emit_signal("widget::redraw_needed")
	end))
	c:connect_signal("property::maximized", function(_)
		btn:emit_signal("widget::redraw_needed")
	end)
	local ret = make_button_wrapper(c)
	ret:add_at(btn, {x=0, y=tbar_button_layout_y_offset})
	return ret
end

local function make_minimize_button(c)
	local btn = make_button(c, "min")
	btn:buttons(awful.button({ }, 1, nil, function()
		btn.state = ""
		c.minimized = not c.minimized
	end))
	local ret = make_button_wrapper(c)
	-- add another padding to the left towards the caption
	ret.forced_width = ret.forced_width + style.button_padding
	ret:add_at(btn, {x=style.button_padding, y=tbar_button_layout_y_offset})
	return ret
end

local function make_titlebar_widgets(c, inner_top, inner_right, inner_bottom, inner_left)
	local top    = wibox.widget.base.make_widget(inner_top)
	local right  = wibox.widget.base.make_widget(inner_right)
	local bottom = wibox.widget.base.make_widget(inner_bottom)
	local left   = wibox.widget.base.make_widget(inner_left)

	top.draw    = make_top(c)
	right.draw  = make_right(c)
	left.draw   = make_left(c)
	bottom.draw = make_bottom(c)

	local function redraw_all()
		top:emit_signal("widget::redraw_needed")
		right:emit_signal("widget::redraw_needed")
		left:emit_signal("widget::redraw_needed")
		bottom:emit_signal("widget::redraw_needed")
	end
	c:connect_signal("focus", redraw_all)
	c:connect_signal("unfocus", redraw_all)

	return top, right, bottom, left
end

local function make_titlebar_top_minimal(c, inner_top)
	local top = wibox.widget.base.make_widget(inner_top)

	top.draw = make_top_minimal(c)

	local function redraw_all()
		top:emit_signal("widget::redraw_needed")
	end
	c:connect_signal("focus", redraw_all)
	c:connect_signal("unfocus", redraw_all)

	return top
end

function titlebar:get_corner_radius()
	return style.corner_radius
end

-- Apply titlebar configuration
-----------------------------------------------------------------------------------------------------------------------
function titlebar:init(args)

	local args = args or {}

	-- enable full transparency below drawn titlebars
	beautiful.titlebar_bg        = "transparent"
	beautiful.titlebar_bg_focus  = "transparent"
	beautiful.titlebar_bg_normal = "transparent"

	-- Add a titlebar if titlebars_enabled is set to true in the rules.
	client.connect_signal("request::titlebars", function(c)

		c._dark_titlebars = false
		for _, rule in pairs(style.dark_mode_rules) do
			if awful.rules.match(c, rule) then
				c._dark_titlebars = true
				break
			end
		end

		-- mouse click actions
		local build_actions = args.title_actions or default_title_actions
		local title_actions = build_actions(c)

		-- Titlebar buttons
		local button_panel = wibox.layout.fixed.horizontal()

		-- add minimize and maximize buttons only for non-dialog type clients
		local is_dialog = (c.type == "dialog") or (c.type == "utility") or c.skip_taskbar or c.modal
		if not is_dialog then
			button_panel:add(make_minimize_button(c))
			button_panel:add(make_maximize_button(c))
		end

		local close_button = make_close_button(c)
		button_panel:add(close_button)

		local button_count = 0
		for _ in pairs(button_panel.children) do button_count = button_count + 1 end

		local left_margin
		if style.caption_centered then
			-- mirror right margin introduced by titlebar buttons
			left_margin = button_count * close_button.forced_width
		else
			left_margin = style.caption_margin
		end

		local inner_top = wibox.widget.base.make_widget_declarative {
			layout = wibox.layout.align.horizontal,
			expand = "inside",
			nil,  -- leftmost widget: invisible, takes no space
			{     -- center widget: client title
				layout = wibox.layout.margin(),
				buttons = title_actions,
				style.caption_enabled and make_title_caption(c) or nil,
				left = left_margin
			},
			button_panel  -- rightmost widget: titlebar button panel
		}

		if style.compositor_draws_shadows_and_corners then
			local top = make_titlebar_top_minimal(c, inner_top)
			awful.titlebar(c, {size = style.top_height}):set_widget(top)
		else
			local top, right, bottom, left = make_titlebar_widgets(c, inner_top)
			awful.titlebar(c, {size = style.top_height}):set_widget(top)
			awful.titlebar(c, {size = 1, position = "right"}):set_widget(right)
			awful.titlebar(c, {size = 1, position = "left"}):set_widget(left)
			awful.titlebar(c, {size = style.bottom_height, position = "bottom"}):set_widget(bottom)
		end
	end)

	if not style.compositor_draws_shadows_and_corners then
		helpers.activate_titlebar_retraction()
	end
end

-- End
-----------------------------------------------------------------------------------------------------------------------
return titlebar