# M4he's Awesome WM config

This is my personal configuration based on [worron](https://github.com/worron)'s fantastic [awesome-config](https://github.com/worron/awesome-config) utilizing his [Red Flat extension library](https://github.com/worron/redflat) for [Awesome WM](http://awesome.naquadah.org).
A huge shout-out to this guy for creating this collection of some of the most aesthetically pleasing widgets and themes for AwesomeWM 4!

## Previews

![](https://gitlab.com/M4he/awesome-config/wikis/images/axent-style-2021.jpg)

(GTK2/3 = [Stylish recolored](https://gitlab.com/M4he/stylish-gtk-theme), Icons = [Reversal](https://github.com/yeyushengfan258/Reversal-icon-theme), Wallpaper = [Plasma 5.10](https://www.pling.com/p/1170141/), Cursor = Breeze)

---

![](https://gitlab.com/M4he/awesome-config/wikis/images/axent-bright-2021.jpg)

(GTK2/3 = [Stylish recolored](https://gitlab.com/M4he/stylish-gtk-theme), Icons = [elementary-xfce](https://github.com/shimmerproject/elementary-xfce), Wallpaper = [from Unsplash](https://unsplash.com/photos/O7Fd4L72BkA), Cursor = Breeze)

The bright config can be activated by the following changes to `themes/axent/theme.lua` (the first statement has a fixed place in the file, it's commented out per default; don't move it!):

```lua
theme = require('themes.axent.colors.bright')
```
```lua
theme.titlebar_theme = "macish"
```

## Requirements

- `bash`
    - shell used for most of the scripts
- `rofi`
    - main application launcher and custom script caller
- `xrandr`
    - enables screen arrangement options via the **Mnemonic Prompt** (see below)
- `picom`
    - compositor to enable transparency effects; most transparency effects can be disabled in the theme file
    - used for titlebar rendering of shadows and rounded corners
    - see the [titlebars section](#titlebars) and [my configuration recommendations](#picom-configuration-recommendations) below
- `redshift` (optional)
    - used by the external monitor brigthness control script for the `_G.is_laptop` config (see below) and device-specific color temperature adjustments
- `hsetroot` (optional)
    - used within `axent/env-config:reload_awesome()` to black out the wallpaper upon awesome restart to mitigate screen artifacts when screen arrangement changes
- fonts:
    - [`Roboto`](https://fonts.google.com/specimen/Roboto)
    - [`Roboto Condensed`](https://fonts.google.com/specimen/Roboto+Condensed)
    - [`Play`](https://fonts.google.com/specimen/Play)
    - [`Iosevka`](https://github.com/be5invis/Iosevka)


## Customization

### General Hints

Admittedly, this configuration is huge.
Below is a list of the most important core aspects of this configuration's structure to get you started.
I recommend you to explore the rest at your own discretion.

- the configuration base is the `rc-axent.lua` file which imports all other modules
    - functionality-related stuff is mostly located in `axent/`
    - the majority of styling-related stuff resides in `themes/axent/`
- `axent/env-config.lua` allows to define default applications like terminal, file-manager etc. as well as the main modifier key
- `axent/rules-config.lua` is a collection of all `awful.rules` that are be applied to clients
- `axent/alias-config.lua` defines how application names are abbreviated on the panel (use `xprop` to grab the class name, 2nd value)
- `axent/menu-config.lua` defines the root menu structure
- `axent/layout-config.lua` defines the list of enabled layouts

### The `_G.is_laptop` flag

There is a `_G.is_laptop` boolean flag that should be set directly in `rc.lua`. It is useful for differentiating between two machines with different needs (e.g. desktop pc vs. laptop - hence the name).
 
- the flag loads different keybindings, autostart files and scripts depending on the value, specifically:
    - `axent/keys-config.lua` vs. `axent/keys-config-laptop.lua`
    - `scripts/*.sh` vs. `scripts/laptop/*.sh`
    - special sections in `rc-axent.lua` and other files (simply `grep` for the flag)
    - extends `theme/axent/theme.lua` with values from `theme/axent/hidpi.lua`
- `axent/keys-config-laptop.lua` is a patcher for the corresponding `keys-config.lua` instead of a complete dupe, so you only need to define differences in there

### Recommended Adjustments

You should *at least* adjust the following files to make sure the configuration fits your environment:

- `rc.lua`, set the `_G.is_laptop` flag
- `axent/etc/autostart.*` files
- `axent/rc-config.lua`
- `axent/env-config.lua`
- `axent/menu-config.lua`
- `axent/keys-config.lua` and `axent/keys-config-laptop.lua`
- `axent/rules-config.lua`

### Titlebars

This framework brings several titlebar designs located in `redflat-extra/titlebar-*.lua`.
The desired titlebar design may be selected via `theme.titlebar_theme` in the `themes/axent/theme.lua`.

Note that many of the designs require a compositor to handle corner rounding and shadow rendering\*.
Here are some recommended `picom` settings that work well for the titlebar modules:

```
shadow = true;
shadow_color = "#333333";
shadow-opacity = 0.6;
shadow-radius = 15;
shadow-offset-x = -15;
shadow-offset-y = -12;
shadow-exclude = [
    "_GTK_FRAME_EXTENTS@:c",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_MAXIMIZED'",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_FULLSCREEN'",
    "class_g ?= 'awesome' && window_type = 'dock'",
    "class_g ?= 'awesome' && window_type = 'utility'",
    "window_type *= 'dropdown_menu'",
    "window_type *= 'dnd'",
    "window_type *= 'utility'"
];

corner-radius = 4;
rounded-corners-exclude = [
    "window_type = 'dock'",
    "window_type = 'desktop'",
    "window_type *= 'dnd'",
    "class_g != 'awesome' && window_type = 'popup_menu'",
    "class_g ?= 'awesome' && window_type = 'splash'",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_MAXIMIZED'"
];
```

(adjust to your liking)

For the `axent.colors.bright` variant and `macish` titlebars enabled in `theme.lua` I recommend:

```
corner-radius = 4;
shadow = true;
shadow_color = "#333333";
shadow-opacity = 0.6;
shadow-radius = 8;
shadow-offset-x = -8;
shadow-offset-y = -7;
```


\* notable exceptions that don't require compositor-based corner rounding and shadow rendering are:

- `macish` when setting its `compositor_draws_shadows_and_corners` to `false`
    - `macish` includes a sophisticated alpha-shaded smooth corner rounding and fake shadow (1px) drawing which can be utilized by setting this flag to `false`, requires compositor for handling transparency
- `mini` when specifying `corner_radius` greater `0` which will apply a rough window shape clipping with rounded corners (will be pixelated!)
- `side` when specifying `corner_radius` greater `0` which will apply a rough window shape clipping with rounded corners (will be pixelated!)


### Rofi

- this configuration uses Rofi as main application launcher
- Rofi is themed by the Awesome configuration and colored accordingly, see `axent/rofi-config.lua` for details
     - depending on your Rofi version, you might need to replace `#sidebar` with `#mode-switcher` in that file!
- Rofi is extended with additional commands using the `scripts/rofi-extra.sh` or `scripts/laptop/rofi-extra.sh`
    - commands defined therein are merged with the general application list in Rofi's first tab

---

## Usage Documentation

### Hot Edges

**To disable hot edges, remove `edges:init()` from the `rc-axent.lua` file.**

| Hot corner/edge | Description |
| ------ | ------ |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/hot-corner-edges.png) | Switch between the last two tags by clicking *the upper corners* with *any* mouse button. switch tags left and right by pressing the *corresponding* mouse button at the left or right *screen edge* outside of the hot corners. |

The hot corners/edges react on mouse clicks at the first/last pixel of the screen edge.
See `edges-config.lua` in each config's subdirectory for details and corner threshold settings.
**Hot corners/edges are only active on the primary monitor!**

### Client Menu

The client menu can be accessed in two ways:

- right-clicking a window's titlebar
- **[alt]** + middle mouse button above a window

The client menu provides the following functionalities:

![](https://gitlab.com/M4he/awesome-config/wikis/doc/clientmenu.png)

The visual representation of tags as colored squares in the client menu can be disabled via the `enable_tagline` option of the widget's style in the theme configuration. If disabled, classic menus for tag handling will be shown instead, similar to when right-clicking the taskbar entry.

### Hotkeys

The following documents the hotkey combinations. Keep in mind that this configuration is based on a **QWERTZ** keyboard layout! Adjust the keys for other layouts.

| Hotkey combination | Action |
| ------ | ------ |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/launch-rofi.png) | Launch the app launcher (Rofi) via **[meta]** (a.k.a. Windows/Super key) |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/launch-terminal.png) | Launch the terminal via **[alt] + [enter]** |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/launch-file-manager.png) | Launch the file manager via **[alt] + [shift] + [enter]** |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/restart-awesome.png) | Restart **awesome** via **[ctrl] + [alt] + [R]**|

#### Client and Tag handling

| Hotkey combination | Action(s) |
| ------ | ------ |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/window-switch-wasd.png) | Switch client (window) by direction via **[alt] + [W]**, **[A]**, **[S]**, **[D]** |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/screen-switch-wasd.png) | Switch screen by direction via **[alt] + [shift] + [W]**, **[A]**, **[S]**, **[D]** (will move cursor and focus client on target screen, for multi-monitor setups) |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/window-switch-by-history.png) | Switch client (window) by history via **[alt] + [tab]**, use the **[alt] + [^]** combination to switch back and forth between the current and last |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/window-actions.png) | Control client (window) via  **[alt] + [Q]**, **[Y]**, **[F]**, **[N]**, **[M]**|
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/window-flags.png) | Toggle client (window) flags |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/tag-switch.png) | Switch tag via **[alt] + [1]**, **[2]**, etc. use the **[alt] + [Esc]** combination to switch back and forth between the current and last |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/tag-toggle.png) | Toggle tag for client (window) via **[ctrl] + [alt]**, use **[ctrl] + [alt] + [Shift]** to move client instead |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/switch-layout.png) | Switch tiling layout for current tag via **[alt] + [space]**, use **[alt] + [shift] + [space]** for reverse order |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/widget-hotkeys.png) | Launch various tool widgets via **[alt] + [<]**, **[E]**, **[X]**, **[C]**, **[V]** |

#### Navigator Mode

Navigator Mode may be entered on tiling layouts via **[Alt] + [G]**.

| Hotkey combination | Action(s) |
| ------ | ------ |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/navmode-enter.png) | Enter **Navigator Mode** via **[alt] + [G]** |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/navmode-leave.png) | Exit **Navigator Mode** via **[Esc]** |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/navmode-select.png) | **Navigator Mode**: **douple tap** = select client (window), **tap X, tap Y** = swap clients X and Y |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/navmode-stack-geo.png) | **Navigator Mode:** change stack geometries via **[W]**, **[A]**, **[S]**, **[D]** |
| ![](https://gitlab.com/M4he/awesome-config/wikis/doc/hotkeys/navmode-stack-size.png) | **Navigator Mode:** change stack sizes via **[Q]**, **[E]** and **[Y]**, **[X]** |

#### Mnemonic Prompt

The **Mnemonic Prompt** uses mnemonic key sequences like Emacs to allow complex actions with simple key sequences.

| Combination | Action(s) |
| ------ | ------ |
| **[alt] + [C]** | Show **Mnemonic Prompt** |
| [alt] + [C], **[alt] + [F1]** | Show cheatsheet for **Mnemonic Prompt** |
|||
| [alt] + [C], **[M], [O]** | **M**onitor, set 2nd monitor **O**ntop of 1st |
| [alt] + [C], **[M], [E]** | **M**onitor, **E**xtend layout to 2nd monitor |
| [alt] + [C], **[M], [P]** | **M**onitor, only output to **P**rimary monitor |
| [alt] + [C], **[M], [S]** | **M**onitor, only output to **S**econdary monitor |
| [alt] + [C], **[M], [M]** | **M**onitor, switch to **M**irror mode |
|||
| [alt] + [C], **[K], [A]** | **K**ill **A**ll clients on current tag |
| [alt] + [C], **[K], [F]** | **K**ill **F**ocused client |
|||
| [alt] + [C], **[N], [A]** | Mi**N**imize **A**ll clients on current tag |
| [alt] + [C], **[N], [F]** | Mi**N**imize **F**ocused client |
| [alt] + [C], **[N], [E]** | Mi**N**imize all clients on current tag **E**xcept the focused one |
|||
| [alt] + [C], **[R], [A]** | **R**estore **A**ll clients on current tag |
| [alt] + [C], **[R], [F]** | **R**estore the last **F**ocused client |
|||
| [alt] + [C], **[C], [P]** | change **C**lient default **P**lacement between master and slave stack (toggle) |
|||
| [alt] + [C], **[Tab], [1..9]** | move currently focused client to tag specified by the number key pressed after [Tab] |
| [alt] + [C], **[Tab], [Tab]** | activate client mover; focused client will be highlighted and can be moved to any tag using the number keys, [Esc] or [space] will end moving and place the client |
|||
| [alt] + [C], **[B], [0..9]** | change **B**rightness of external monitor to 10\**k*, where *k* is the number key pressed, pressing *0* will reset to 100% (laptop only\*) |
| [alt] + [C], **[T], [B]** | **T**oggle **B**luetooth on/off (laptop only\*) |
| [alt] + [C], **[T], [W]** | **T**oggle **W**iFi on/off (laptop only\*) |

\* only available when `_G.is_laptop` is set to `true` in the `rc.lua` file.
The bluetooth and WiFi toggles require passwordless sudo permissions for `rfkill`, 
see `scripts/rfkill.sh` for details.

## Tips

### Picom configuration recommendations

```
backend = "glx";
glx-no-stencil = true;
vsync = true;

fading = true;
fade-in-step = 0.2;
fade-out-step = 0.2;
```

#### Using blur with Picom

This configuration includes optional transparency for some elements (such as wibar and widgets) that can make use of blur effects. The blur options require Picom's `--experimental-backends` flag.
Adhere to the following steps to change to Picom's experimental mode and activate the transparency:

1. within `axent/toggles-config.lua` change "`picom.sh on`" to "`picom.sh on-experimental`".

2. within `rc-axent.lua` set `" --experimental"` as `picom_args`.

3. make sure to set "`theme.transparency_enabled = true`" in `themes/axent/theme.lua`.

4. finally, add the following to your `~/.config/picom.conf`:

```
blur: {
  method = "dual_kawase";
  strength = 5;
  background = false;
  background-frame = false;
  background-fixed = false;
}

blur-background-exclude = [
    "window_type = 'desktop'",
    "class_g != 'awesome' && class_g != 'Rofi'",
    "_GTK_FRAME_EXTENTS@:c"
];
```

Feel free to increase the `strength` parameter of the blur method if your GPU is powerful enough.