local helpers = require("redflat-extra.helpers")

local theme = {}

theme.color = {
    main      = "#7FBF00",   -- main accent color
    gray      = "#B0B1B3", -- used for inactive elements and bar trunks
    wibox     = "#F6F6F7",   -- border, panel and general background color
    icon      = "#525252",   -- icons in menus
    text      = "#525252",   -- text in menus and titlebars
    urgent    = "#FF4070",   -- urgent window highlight in taglist, tasklist also volume mute
    highlight = "#FFFFFF",   -- text when highlighted in menu
    empty     = "#FFFFFF66", -- circle tag empty color

    border    = "#F6F6F7",   -- tooltip border
    shadow1   = "#D8D9D9", -- separator dark side
    shadow2   = "transparent", -- separator bright side
    shadow3   = "#808080",   -- buttons outer border
    shadow4   = "#6B6B6B",   -- buttons inner border

    secondary = "#A8ABB3",
    border_normal = "#33333366",
    border_focus  = "#33333366",
    panel_border = "#55555533",
}

-- bg used for custom widgets (e.g. appswitcher, top)
theme.color.bg        = helpers.scale_hex_color(theme.color.wibox, 0.95 )
-- alternating lines for 'bg'
theme.color.bg_second = helpers.scale_hex_color(theme.color.wibox, 0.9 )

return theme