local theme = {}

theme.color = {
    main      = "#FF4D4D",
    gray      = "#777777",  -- used for inactive elements and bar trunks
    bg        = "#333438",  -- bg used for custom widgets (e.g. appswitcher, top)
    bg_second = "#3C3E42",  -- alternating lines for 'bg'
    wibox     = "#252629",  -- border, panel and general background color
    icon      = "#EEEEEE",  -- icons in menus
    text      = "#EEEEEE",  -- text in menus and titlebars
    urgent    = "#FF4070",  -- urgent window highlight in taglist, tasklist also volume mute
    highlight = "#252629",  -- text when highlighted in menu
    empty     = "#FFFFFF66",  -- circle tag empty color

    border    = "#454545",  -- tooltip border
    shadow1   = "#FFFFFF19",  -- separator dark side
    shadow2   = "#FFFFFF19",  -- separator bright side
    shadow3   = "#808080",  -- buttons outer border
    shadow4   = "#626A6B",  -- buttons inner border

    secondary = "#A8ABB3",
    title_off = "#B3B3B3",   -- unfocused titlebar color
    border_normal = "#999999",
    border_focus  = "#70A800",
    panel_border = "#454545",

    window_base_active   = "#F7F7F7",
    window_base_inactive = "#F7F7F7",
    window_accent_inactive = "#DBDBDB",
    window_title_active = "#333333",
    window_title_inactive = "#AAAAAA",
}

return theme