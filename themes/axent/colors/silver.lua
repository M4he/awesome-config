local theme = {}

theme.color = {
    main      = "#60BF30",
    gray      = "#B3B3B3",  -- used for inactive elements and bar trunks
    bg        = "#E0E0E0",  -- bg used for custom widgets (e.g. appswitcher, top)
    bg_second = "#DBDBDB",  -- alternating lines for 'bg'
    wibox     = "#EBEBEB",  -- border, panel and general background color
    icon      = "#5C5C5C",  -- icons in menus
    text      = "#5C5C5C",  -- text in menus and titlebars
    urgent    = "#FF4070",  -- urgent window highlight in taglist, tasklist also volume mute
    highlight = "#FFFFFF",  -- text when highlighted in menu
    empty     = "#B3B3B3",  -- circle tag empty color

    border    = "#B3B3B3",  -- tooltip border
    shadow1   = "#D1D1D1",  -- separator dark side
    shadow2   = "#FAFAFA",  -- separator bright side
    shadow3   = "#808080",  -- buttons outer border
    shadow4   = "#B3B3B3",  -- buttons inner border

    secondary = "#B3B3B3",
    border_normal = "#999999",
    border_focus  = "#70A800",
    panel_border = "#B3B3B3",
}

return theme