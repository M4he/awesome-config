local theme = {}

theme.color = {
    main      = "#CD664D",
    gray      = "#B0AB96",  -- used for inactive elements and bar trunks
    bg        = "#B4AF9A",  -- bg used for custom widgets (e.g. appswitcher, top)
    bg_second = "#BFBAA4",  -- alternating lines for 'bg'
    wibox     = "#DAD4BB",  -- border, panel and general background color
    icon      = "#4E4B42",  -- icons in menus
    text      = "#4E4B42",  -- text in menus and titlebars
    urgent    = "#5C9499",  -- urgent window highlight in taglist, tasklist also volume mute
    highlight = "#DAD4BB",  -- text when highlighted in menu
    empty     = "#B0AB96",  -- circle tag empty color

    border    = "#8C8877",  -- tooltip border
    shadow1   = "#B5B09B",  -- separator dark side
    shadow2   = "#B5B09B",  -- separator bright side
    shadow3   = "#808080",  -- buttons outer border
    shadow4   = "#B0AB96",  -- buttons inner border

    title_off = "#B0AB96",
    secondary = "#635F54",
    border_normal = "#8E8A79",
    border_focus  = "#8E8A79",
    panel_border = "#8E8A79",
}

return theme