-----------------------------------------------------------------------------------------------------------------------
--                                           aXent theme HiDPI config                                                --
-----------------------------------------------------------------------------------------------------------------------
local theme = {}

theme.fonts = {
    main     = "RobotoCondensed 14",        -- main font
    menu     = "RobotoCondensed 14",        -- main menu font
    titlebar = "Play semibold 12",          -- client titlebar font
    tooltip  = "RobotoCondensed 14",        -- tooltip font
    notify   = "RobotoCondensed medium 18", -- redflat notify popup font
    player   = {
        main = "RobotoCondensed medium 16", -- player widget main font
        sub = "RobotoCondensed medium 14",  -- player widget sub font
        time = "RobotoCondensed medium 16", -- player widget current time font
    },
    calendar = {
        clock       = "RobotoCondensed 32",
        date        = "RobotoCondensed 16",
        days        = "RobotoCondensed 14",
        today       = "RobotoCondensed medium 14",
        label       = "RobotoCondensed medium 14",
        header      = "RobotoCondensed 13",
        weeknumbers = "RobotoCondensed 13",
    },
}

theme.float = {}
theme.float.calendar = {}
theme.float.calendar.geometry = { width = 320, height = 420 }

theme.float.player = {}
theme.float.player.geometry        = { width = 520, height = 150 }
theme.float.player.controls_margin = { 0, 0, 20, 10 }
theme.float.player.line_height     = 32

theme.float.controlcenter = {}
theme.float.controlcenter.width        = 420
theme.float.controlcenter.font         = "RobotoCondensed 14"
theme.float.controlcenter.title_font   = "RobotoCondensed 15"
theme.float.controlcenter.title_height = 24
theme.float.controlcenter.entry_height = 48
theme.float.controlcenter.spacing      = 12

theme.titlebars = {}
theme.titlebars.breeze = {}
theme.titlebars.breeze.top_height = 30
theme.titlebars.breeze.button_size = { w = 20.5, h = 20.5 }
theme.titlebars.breeze.icon_size = 22

return theme