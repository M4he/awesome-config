-----------------------------------------------------------------------------------------------------------------------
--                                                Signals config                                                     --
-----------------------------------------------------------------------------------------------------------------------

-- Grab environment
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")

-- Initialize tables and vars for module
-----------------------------------------------------------------------------------------------------------------------
local signals = {}

-- Support functions
-----------------------------------------------------------------------------------------------------------------------

-- resize maximized windows to correctly fit the available space
-- while accounting for useless_gaps, borders and detached wibars
local function fixed_maximized_geometry(c)
	if c.maximized and not c.fullscreen then
		c:geometry({
			x = c.screen.workarea.x,
			y = c.screen.workarea.y,
			height = c.screen.workarea.height - 2 * c.border_width - (beautiful.detached_wibar and 2 * beautiful.useless_gap or 0),
			width = c.screen.workarea.width - 2 * c.border_width
		})
	-- fix up any maximized windows misinterpreting the screen geometry
	elseif c.fullscreen then
		c:geometry({
			x = c.screen.geometry.x,
			y = c.screen.geometry.y,
			height = c.screen.geometry.height,
			width = c.screen.geometry.width
		})
	end
end

local min_w = 160
local min_h = 100
local function ensure_minimum_size(c)
	if not c.size_hints_honor then
		c.width = (c.width < min_w) and min_w or c.width
		c.height = (c.height < min_h) and min_h or c.height
	end
end

local function update_client_shape(c)
	if not beautiful.window_shape or c.fullscreen or c.maximized or c.rule_borderless then
		c.shape = nil
	else
		c.shape = beautiful.window_shape
	end
end

-- for FLOATING layouts and clients
-- push clients away from the screen edges according to the useless_gap value
-- and resize clients to a reasonable preferred size
local gapsize = beautiful.useless_gap*2
local function fixed_floating_geometry(c)
	if c.maximized or c.fullscreen or not c.focusable then return end
	if c.floating or (c.first_tag ~= nil and c.first_tag.layout == awful.layout.suit.floating) then
		local wa = c.screen.workarea

		ensure_minimum_size(c)

		-- client size including borders
		local bordered_w = c.width + 2 * beautiful.border_width
		local bordered_h = c.height + 2 * beautiful.border_width

		-- push away from right edge
		if c.x + bordered_w > wa.x + wa.width - gapsize then
			c.x = (wa.x + wa.width) - gapsize - bordered_w
		end
		-- push away from bottom edge
		if c.y + bordered_h > wa.y + wa.height - gapsize then
			c.y = (wa.y + wa.height) - gapsize - bordered_h
		end

		-- push away from left edge
		if c.x < wa.x + gapsize then c.x = wa.x + gapsize end
		-- push away from top edge
		if c.y < wa.y + gapsize then c.y = wa.y + gapsize end
	end
end

-- Build table
-----------------------------------------------------------------------------------------------------------------------
function signals:init(args)

	local args = args or {}
	local env = args.env

	local reinit_awesome = env.reload_awesome or awesome.restart

	-- actions on every application start
	client.connect_signal(
		"manage",
		function(c)
			-- put client at the end of list
			if env.set_slave then awful.client.setslave(c) end

			-- startup placement
			if awesome.startup
			   and not c.size_hints.user_position
			   and not c.size_hints.program_position
			then
				awful.placement.no_offscreen(c, {honor_workarea=true})
			end

			fixed_maximized_geometry(c)
			fixed_floating_geometry(c)
			update_client_shape(c)
		end
	)

	client.connect_signal(
		"property::maximized",
		function(c)
			if c.maximized then
				fixed_maximized_geometry(c)
			else
				fixed_floating_geometry(c)
			end
			update_client_shape(c)
		end
	)

	client.connect_signal(
		"property::fullscreen",
		function(c)
			if not c.fullscreen then fixed_floating_geometry(c) end -- fix geometry if entering floating mode
			update_client_shape(c)
		end
	)

	-- don't allow maximized windows move/resize themselves
	client.connect_signal(
		"request::geometry",
		function(c)
			ensure_minimum_size(c)
		end
	)

	-- refresh useless_gap sizing when layout changes between max and non-max
	tag.connect_signal(
		"property::layout",
		function(t)
			t.gap = t.layout.name == "max" and 0 or beautiful.useless_gap
		end
	)

	-- Multiscreen handling
	-------------------------------------------------------------------------------------------------------------------

	-- restart awesome if the primary screen changes
	-- we want to reorder our screens here otherwise
	-- the wibars and tags might stay on the wrong
	-- monitor
	screen.connect_signal("primary_changed",
		function(s)
			reinit_awesome()
		end
	)
	-- restart Awesome on any other screen changes
	-- (mandatory for redflat widgets)
	screen.connect_signal("removed", function()
		reinit_awesome()
	end)
	screen.connect_signal("added", function()
		reinit_awesome()
	end)
	screen.connect_signal("property::geometry", function()
		reinit_awesome()
	end)
end

-- End
-----------------------------------------------------------------------------------------------------------------------
return signals
