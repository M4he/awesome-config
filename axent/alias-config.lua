-----------------------------------------------------------------------------------------------------------------------
--                                      Application aliases for tasklist widget                                      --
-----------------------------------------------------------------------------------------------------------------------

local appnames = {}

appnames["Firefox"] = "WWW"
appnames["Firefox-esr"] = "WWW"
appnames["firefox"] = "WWW"
appnames["Inkscape"] = "INK"
appnames["Ld-linux-x86-64.so.2"] = "INK" -- Inkscape AppImage version
appnames["Hexchat"] = "IRC"
appnames["konversation"] = "IRC"
appnames["Audacious"] = "AUDCS"
appnames["Sublime_text"] = "SUBL"
appnames["Gnome-system-monitor"] = "SYSMN"
appnames["Gnome-calculator"] = "GCALC"
appnames["Galculator"] = "GALC"
appnames["Gimp-2.10"] = "GIMP"
appnames["Easytag"] = "ETAG"
appnames["Gcolor2"] = "COLOR"
appnames["Mate-color-select"] = "COLOR"
appnames["Mcomix"] = "COMIX"
appnames["URxvt"] = "TERM"
appnames["VirtualBox"] = "VBOX"
appnames["Keepassx"] = "KPASS"
appnames["keepassxc"] = "KPASS"
appnames["KeePassXC"] = "KPASS"
appnames["Simplescreenrecorder"] = "REC"
appnames["Pidgin"] = "PIDGN"
appnames["thunderbird"] = "MAIL"
appnames["Thunderbird"] = "MAIL"
appnames["Xfce4-screenshooter"] = "SCROT"
appnames["Nautilus"] = "FILES"
appnames["Org.gnome.Nautilus"] = "FILES"
appnames["Pcmanfm"] = "FILES"
appnames["Evince"] = "EVINCE"
appnames["jetbrains-pycharm-ce"] = "PYCRM"
appnames["jetbrains-pycharm"] = "PYCRM"
appnames["libreoffice-draw"] = "DRAW"
appnames["libreoffice-writer"] = "WRITR"
appnames["libreoffice-impress"] = "IMPRS"
appnames["libreoffice-calc"] = "CALC"
appnames["File-roller"] = "ARCHV"
appnames["Pavucontrol"] = "PAVOL"
appnames["Oblogout"] = "EXIT"
appnames["Gcr-prompter"] = "PIN"
appnames["Gthumb"] = "PICS"
appnames["Xfce4-taskmanager"] = "TASKM"
appnames["linphone"] = "PHONE"
appnames["org.remmina.Remmina"] = "REMOT"

return appnames

