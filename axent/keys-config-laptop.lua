-- Patch the regular keys-config with laptop-specific definitions

local awful = require("awful")
local gears = require("gears")
local redflat = require("redflat")
local base = require("axent.keys-config")

local hotkeys = {}

setmetatable(hotkeys, {__index = base})  -- first, inherit everything from base

-- returns true when table `t` contains all elements from table `t2`
local function table_contains_all_from_table(t, t2)
	local does_match = true
	for _, e in ipairs(t2) do
		if not t[e] then
			does_match = false
			break
		end
	end
	return does_match
end

-- replaces keybinding definitions in the table 'target' with those of the
-- table 'patches'
-- if the function (third element) of a keybinding in 'patches' is nil, then
-- the keybinding definition will be deleted from 'target' entirely
local function patch_keytable(target, patches)
	for _, k in ipairs(patches) do
		local match_found = false
		-- we reverse iterate the table so that a table.remove() does
		-- not mess up the order during traversal
		for i2 = #target, 1, -1 do
			local k2 = target[i2]
			-- amount of modifiers equals and key equals
			if #k[1] == #k2[1] and k[2] == k2[2] then
				-- check if modifiers match
				if table_contains_all_from_table(k[1], k2[1]) then
					if k[3] == nil then
						-- remove entry
						table.remove(target, i2)
						match_found = true
					else
						-- replace entry
						target[i2] = k
						match_found = true
					end
					break
				end
			end
		end
		-- no match found, append entry
		if not match_found then
			table.insert(target, k)
		end
	end
end

local _setup_keyseq = base.setup_keyseq
base.setup_keyseq = function(args)

	local keyseq = _setup_keyseq(args)

	-- patch existing keyseqs
	for _, k in ipairs(keyseq[3]) do
		-- patch monitor group
		if k[2] == "m" then
			k[3] = {
				{
					{}, "p", function() awful.spawn.with_shell("bash " .. awful.util.get_configuration_dir() .. "scripts/laptop/xrandr.sh primary") end,
					{ description = "Only activate PRIMARY screen", group = "Monitor management", keyset = { "p" } }
				},
				{
					{}, "s", function() awful.spawn.with_shell("bash " .. awful.util.get_configuration_dir() .. "scripts/laptop/xrandr.sh secondary") end,
					{ description = "Only activate SECONDARY screen", group = "Monitor management", keyset = { "s" } }
				},
				{
					{}, "e", function() awful.spawn.with_shell("bash " .. awful.util.get_configuration_dir() .. "scripts/laptop/xrandr.sh extend") end,
					{ description = "Extend the internal screen to the right", group = "Monitor management", keyset = { "e" } }
				},
				{
					{}, "m", function() awful.spawn.with_shell("bash " .. awful.util.get_configuration_dir() .. "scripts/laptop/xrandr.sh mirror") end,
					{ description = "Mirror both screens", group = "Monitor management", keyset = { "m" } }
				},
				{
					{}, "o", function() awful.spawn.with_shell("bash " .. awful.util.get_configuration_dir() .. "scripts/laptop/xrandr.sh ontop") end,
					{ description = "Set the external screen ontop", group = "Monitor management", keyset = { "o" } }
				},
			}
		end
	end

	-- additional keyseqs
	local keyseq_ext = {
		{ {}, "b", {}, {} },   -- #1 monitor brightness group
		{ {}, "t", {}, {} },   -- #2 toggles group
	}

	-- external monitor brightness
	for i = 0, 9 do
		local ik = tostring(i)
		table.insert(keyseq_ext[1][3], {
			{}, ik, function()
				local br = "1.0"
				if i == 0 then
					br = "1.0"
				else
					br = "0." .. ik
				end
				awful.spawn.with_shell("bash " .. awful.util.get_configuration_dir() .. "scripts/laptop/external-brightness.sh " .. br)
			end,
			{ description = "Set external monitor brightness level to " .. ik, group = "Monitor management", keyset = { ik } }
		})
	end

	-- various toggles
	keyseq_ext[2][3] = {
		{
			{}, "b", function() awful.spawn.with_shell("bash " .. awful.util.get_configuration_dir() .. "scripts/rfkill.sh bt-toggle") end,
			{ description = "Toggle bluetooth", group = "Toggles", keyset = { "b" } }
		},
		{
			{}, "w", function() awful.spawn.with_shell("bash " .. awful.util.get_configuration_dir() .. "scripts/rfkill.sh wifi-toggle") end,
			{ description = "Toggle WiFi", group = "Toggles", keyset = { "w" } }
		},
	}

	keyseq[3] = gears.table.join(keyseq[3], keyseq_ext)

	return keyseq
end

local _setup_tile_keys = base.setup_tile_keys
base.setup_tile_keys = function(args)
	local layout = _setup_tile_keys(args)
	local patches = {
		{
			{}, "w", function () awful.client.incwfact( 0.075) end,
			{ description = "Increase window factor of a client", group = "Layout" }
		},
		{
			{}, "s", function () awful.client.incwfact(-0.075) end,
			{ description = "Decrease window factor of a client", group = "Layout" }
		}
	}
	patch_keytable(layout, patches)
	return layout
end

local _setup_root_keys = base.setup_root_keys
base.setup_root_keys = function(args)
	local root = _setup_root_keys(args)
	local env = args.env or {}
	local patches = {
		{ -- remove
			{}, "F3", function() end,
			{}
		},
		{
			{ }, "XF86Search", function() awful.spawn(env.rofi .. " window") end,
			{ description = "Show Window Lookup", group = "Main" }
		},
		{
			{}, "XF86MonBrightnessUp", function() awful.spawn("brightnessctl set 5%+") end,
			{ description = "Control screen brightness", group = "Main" }
		},
		{
			{}, "XF86MonBrightnessDown", function() awful.spawn("brightnessctl set 5%-") end,
			{ description = "Control screen brightness", group = "Main" }
		},
		{
			{ env.mod }, "l", function() awful.spawn(env.screenlock) end,
			{ description = "Lock screen", group = "Main" }
		},
		{
			{}, "XF86PowerOff", function() redflat.service.logout:show() end,
			{ description = "Show logout / power control prompt", group = "Main" }
		},
		--- SFW MODES
		{
			{ env.mod, "Control" }, "s", function() awful.spawn(env.home .. "/.styles/stylechanger.py SFW") end,
			{} -- hidden key
		},
		{
			{ env.mod, "Shift", "Control" }, "s", function() awful.spawn(env.home .. "/.styles/stylechanger.py") end,
			{} -- hidden key
		},
	}
	patch_keytable(root, patches)
	return root
end

return hotkeys
