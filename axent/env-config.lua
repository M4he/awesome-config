-----------------------------------------------------------------------------------------------------------------------
--                                                  Environment config                                               --
-----------------------------------------------------------------------------------------------------------------------

local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local naughty = require("naughty")
local redflat = require("redflat")

-- Initialize tables and vars for module
-----------------------------------------------------------------------------------------------------------------------
local env = {}

-- Initialize environment settings and theme
-----------------------------------------------------------------------------------------------------------------------
function env:init(args)

	-- init vars
	local args = args or {}
	local theme = args.theme or "mahe"

	-- environment vars: general settings
	self.mod        = args.mod or "Mod1"
	self.home       = os.getenv("HOME")
	self.themedir   = gears.filesystem.get_configuration_dir() .. "themes/" .. theme
	self.set_slave  = true -- whether new clients will enter the slave stack instead of master

	-- environment vars: tools
	self.terminal   = args.terminal   or "tilix"
	self.sysmon     = args.sysmon     or "xfce4-taskmanager"
	self.screenlock = args.screenlock or "mate-screensaver-command --lock"
	self.screenshot = args.screenshot or "xfce4-screenshooter -d 0 -f"
	self.calculator = args.calculator or "gnome-calculator"
	self.fm         = args.fm         or "nautilus -w"
	self.mail       = args.mail       or "thunderbird"
	self.player     = args.player     or "audacious"
	self.upgrades   = args.upgrades   or "echo 'FIXME'"
	self.rofi       = "rofi" -- will be overriden as full command by rofi-config module after initialization

	self.reload_awesome = function()
		-- blank wallpaper
		awful.spawn.with_shell("hsetroot -solid '#000000'")
		awesome.restart()
	end

	-- theme setup
	beautiful.init(env.themedir .. "/theme.lua")

	-- naughty notification config
	naughty.config.padding = beautiful.useless_gap and 2 * beautiful.useless_gap or 0
	if beautiful.naughty then
		naughty.config.defaults         = redflat.util.table.merge(naughty.config.defaults, beautiful.naughty.base)
		naughty.config.presets.normal   = redflat.util.table.merge(beautiful.naughty.base, beautiful.naughty.normal)
		naughty.config.presets.critical = redflat.util.table.merge(beautiful.naughty.base, beautiful.naughty.critical)
		naughty.config.presets.low      = redflat.util.table.merge(beautiful.naughty.base, beautiful.naughty.low)
	end
	-- always show notifications on the primary screen
	naughty.config.notify_callback = function(notify_args)
		notify_args.screen = screen.primary
		return notify_args
	end
end

-- End
-----------------------------------------------------------------------------------------------------------------------
return env
