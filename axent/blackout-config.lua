-----------------------------------------------------------------------------------------------------------------------
--                                               blackout config                                                      --
-----------------------------------------------------------------------------------------------------------------------

-- This is a helper module that blacks out the portion of the wallpaper that is within the workarea on max
-- layouts in order to mitigate bleedthrough effects for transparent or rounded window borders on max layouts

-- Grab environment
local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local redflat = require("redflat")

-- Initialize tables and vars for module
-----------------------------------------------------------------------------------------------------------------------
local blackout = {}

function blackout:init()

    self.boards = {}
    for s in screen do
        local wa = s.workarea
        self.boards[s] = wibox({
            x = wa.x,
            y = wa.y,
            width = wa.width,
            height = wa.height,
            bg = "black",
            border_width = 0,
            ontop = false,
            type = "desktop",
        })
        self.boards[s].visible = false
    end
    tag.connect_signal(
        "property::selected", function(t)
            s = t.screen
            self.boards[s].visible = t.layout.name == "max"
        end
    )
end

-- End
-----------------------------------------------------------------------------------------------------------------------
return blackout
