### Intel P-State Turbo Boost Toggle

The following script is not included in this directory due to requiring passwordless sudo permissions. Create this file manually:

`/usr/local/bin/intel_pstate_turbo`
```bash
#!/bin/bash

function turbo_check() {
    NO_TURBO=$(cat /sys/devices/system/cpu/intel_pstate/no_turbo)
    if [ "$NO_TURBO" == "0" ]
    then
        echo "on"
    else
        echo "off"
    fi
}

function turn_turbo_on() {
    echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo
}

function turn_turbo_off() {
    echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo
}

if [ $# -eq 0 ]
then
    turbo_check
else
    if [ "$1" == "on" ]
    then
        turn_turbo_on
    elif [ "$1" == "off" ]
    then
        turn_turbo_off
    elif [ "$1" == "check" ]
    then
        turbo_check
    fi
fi
```

Make it executable

```
sudo chmod +x /usr/local/bin/intel_pstate_turbo
```

... to enable the usage of the script without password prompt use:

```
sudo visudo
```

and add this entry:

```
ALL ALL = (root) NOPASSWD: /usr/local/bin/intel_pstate_turbo
```



