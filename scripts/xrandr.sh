#!/bin/bash

INT="HDMI-2"
EXT="HDMI-1"
case "$1" in
    primary)
        xrandr --output $INT --primary --auto --output $EXT --off;
        ;;
    secondary)
        xrandr --output $EXT --primary --mode 1920x1080 --rate 60 --output $INT --off;
        ;;
    extend)
        xrandr --output $INT --primary --auto --output $EXT --mode 1920x1080 --rate 60 --right-of $INT;
        ;;
    mirror)
        xrandr --output $INT --primary --auto --output $EXT --mode 1920x1080 --rate 60 --same-as $INT
        ;;
    ontop)
        xrandr --output $EXT --primary --mode 1920x1080 --rate 60 --output $INT --auto --below $EXT;
        ;;
    *)
        echo "WRONG INPUT"
        ;;
esac
