#!/bin/bash

# add the following line to your sudoers file (run `visudo` for editing it)
# %sudo   ALL=NOPASSWD:/usr/sbin/rfkill

# call this script by passing either
# `bt-{check,toggle,on,off}` or `wifi-{check,toggle,on,off}`

# parameter: abstract icon name
# output: absolute path to the resolved icon of the current Gtk theme
function retrieve_gtk_icon() {
    SIZE=48
    python3 -c "import gi; gi.require_version('Gtk', '3.0'); from gi.repository import Gtk; print(Gtk.IconTheme.get_default().lookup_icon('$1', $SIZE, 0).get_filename())"
}

function check_bluetooth() {
    IS_BLOCKED=$(sudo /usr/sbin/rfkill list bluetooth | grep -P '(?<=Soft\sblocked:\s).*' -o)
    if [ "$IS_BLOCKED" == "no" ]
    then
        echo "on"
    else
        echo "off"
    fi
}

function turn_bluetooth_on() {
    sudo /usr/sbin/rfkill unblock bluetooth;
    bluetoothctl power on;
}

function turn_bluetooth_off() {
    bluetoothctl power off;
    sudo /usr/sbin/rfkill block bluetooth;
}

function toggle_bluetooth() {
    STATE=$(check_bluetooth)
    ICON=$(retrieve_gtk_icon 'bluetooth')
    if [ "$STATE" == "on" ]
    then
        turn_bluetooth_off;
        notify-send -i "$ICON" "Bluetooth" "Bluetooth disabled";
    else
        turn_bluetooth_on
        notify-send -i "$ICON" "Bluetooth" "Bluetooth enabled";
    fi
}

function check_wifi() {
    IS_BLOCKED=$(sudo /usr/sbin/rfkill list wlan | grep -P '(?<=Soft\sblocked:\s).*' -o)
    if [ "$IS_BLOCKED" == "no" ]
    then
        echo "on"
    else
        echo "off"
    fi
}

function turn_wifi_on() {
    sudo /usr/sbin/rfkill unblock wlan;
    bluetoothctl power on;
}

function turn_wifi_off() {
    bluetoothctl power off;
    sudo /usr/sbin/rfkill block wlan;
}

function toggle_wifi() {
    STATE=$(check_wifi)
    ICON=$(retrieve_gtk_icon 'wifi')
    if [ "$STATE" == "on" ]
    then
        turn_wifi_off;
        notify-send -i "$ICON" "Wireless LAN" "WiFi disabled";
    else
        turn_wifi_on;
        notify-send -i "$ICON" "Wireless LAN" "WiFi enabled";
    fi
}

if [[ -n "$1" ]]; then
    if [[ "$1" == "bt-toggle" ]]; then
        toggle_bluetooth;
    elif [[ "$1" == "bt-check" ]]; then
        check_bluetooth;
    elif [[ "$1" == "bt-on" ]]; then
        turn_bluetooth_on;
    elif [[ "$1" == "bt-off" ]]; then
        turn_bluetooth_off;
    elif [[ "$1" == "wifi-check" ]]; then
        check_wifi;
    elif [[ "$1" == "wifi-on" ]]; then
        turn_wifi_on;
    elif [[ "$1" == "wifi-off" ]]; then
        turn_wifi_off;
    elif [[ "$1" == "wifi-toggle" ]]; then
        toggle_wifi;
    else
        echo "invalid parameter '$1'"
    fi
else
    echo "no parameters provided"
fi