#!/bin/bash
#
# Toggles PulseAudio low latency loopback by starting/stopping
# a corresponding pacat process; enables loopback across devices

# Input device as per `pacmd list-sources`
INP="alsa_input.pci-0000_00_1f.3.analog-stereo"

# Latency of the pacat command in milliseconds
LAT="30"

turn_off() {
    echo "stopping loopback process"
    pkill -f "^pacat"
    echo "killed loopback process"
}

turn_on() {
    # determine the default output sink via `pacmd`, an asterisk (*) in front of `index` indicates it
    OUT=$(pacmd list-sinks | grep -A1 "* index" | tail -n1 | cut -d'<' -f2 | cut -d'>' -f1)
    echo "identified default output: ${OUT}"
    echo "starting loopback process"
    # the `>/dev/null 2>&1` will discard output and prevents the creation of a `nohup.out`
    nohup sh -c "pacat -r --latency-msec=$LAT -d $INP | pacat -p --latency-msec=$LAT -d $OUT" >/dev/null 2>&1 &
    echo "started loopback process"
}

switch_port_to_mic() {
    pacmd set-source-port "$INP" "analog-input-rear-mic"
}

switch_port_to_linein() {
    pacmd set-source-port "$INP" "analog-input-linein"
}

input_check_is_linein() {
    PORT=$(LC_ALL=C pactl list sources | grep -A60 "Name: $INP" | grep "Active Port" | head -n1 | cut -d' ' -f3)
    if [ "$PORT" == "analog-input-linein" ]
    then
        echo "on"
    else
        echo "off"
    fi
}

input_check_is_mic() {
    PORT=$(LC_ALL=C pactl list sources | grep -A60 "Name: $INP" | grep "Active Port" | head -n1 | cut -d' ' -f3)
    if [ "$PORT" == "analog-input-linein" ]
    then
        echo "off"
    else
        echo "on"
    fi
}

# old toggle function, called when no parameter is passed
do_toggle() {
    # check if process is already running
    pgrep -f "^sh.*pacat"
    STATE=$?
    # if return code equals 0 -> process is running
    if [ $STATE -eq 0 ]
    then
        turn_off
    else
        turn_on
    fi
}


if [ $# -eq 0 ]
then
    do_toggle
else
    if [ "$1" == "check" ]
    then
        (pgrep -f "^sh.*pacat" > /dev/null && echo "on") || echo "off"
    elif [ "$1" == "on" ]
    then
        pgrep -f "^sh.*pacat" > /dev/null && echo "safeguard triggered, loopback already running" && exit 0
        turn_on
    elif [ "$1" == "off" ]
    then
        turn_off
    elif [ "$1" == "input-is-linein" ]
    then
        input_check_is_linein
    elif [ "$1" == "input-is-mic" ]
    then
        input_check_is_mic
    elif [ "$1" == "input-use-mic" ]
    then
        switch_port_to_mic
    elif [ "$1" == "input-use-linein" ]
    then
        switch_port_to_linein
    fi
fi
