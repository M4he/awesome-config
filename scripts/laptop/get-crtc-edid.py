#!/usr/bin/env python3
import sys
import subprocess
import hashlib
import time

# this script returns the CRTC number and an MD5 hash of the EDID of a
# given XRandR screen, separated by a colon ':' character

# example: ./get-crtc-edid.py "DP-1"
# output:  0:bd87021be72ff9de07e93ff41235a435

_display = sys.argv[1]

def get_edid_for_display(display_identifier, retries=50):
    _EDID = ""
    _CRTC = ""
    _try_count = 0

    # sometimes XRandR does not expose the EDID for a few consecutive
    # calls, so we might need to retry a few times
    while not _EDID and _try_count < retries:
        if _try_count > 0:
            time.sleep(0.1)  # stall for a bit
        _try_count += 1

        _flag_edid = False
        _flag_display = False
        _header_indent = -1
        _line_num = 0

        xrandr_output = subprocess.check_output(['xrandr', '--verbose'])
        for line in xrandr_output.decode().split('\n'):
            if (not _flag_edid and _EDID) and _CRTC:
                return _CRTC, _EDID

            _line_num += 1
            if line.startswith("%s connected" % _display):
                _flag_display = True
                continue
            elif "connected" in line:
                _flag_display = False
                continue

            if _flag_display:
                trimmed = line.strip()
                indent = len(line) - len(line.lstrip())

                # EDID section header?
                if trimmed.startswith("EDID:"):
                    _flag_edid = True
                    _header_indent = indent
                    continue

                if _flag_edid:
                    # leaving EDID section?
                    if indent == _header_indent:
                        # if the indent matches the header indent, it means
                        # the next section has begun, effectively terminating
                        # the EDID section
                        _flag_edid = False
                    else:
                        _EDID += str(trimmed).strip('\n')

                if trimmed.startswith("CRTC:"):
                    _CRTC = trimmed.replace("CRTC:", "").strip().rstrip()
                    continue


    return "", ""

# return the MD5 hashsum of the EDID
crtc, edid = get_edid_for_display(_display)
print(crtc + ':' + hashlib.md5(edid.encode()).hexdigest())