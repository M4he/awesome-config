#!/bin/bash

TOUCHPAD_DEVICE="CUST0001:00 06CB:76AF Touchpad"

function tapping_check() {
    VAL=$(xinput list-props "$TOUCHPAD_DEVICE" | grep "Tapping Enabled (" | cut -d':' -f2 | xargs)
    if [ "$VAL" == "1" ]
    then
        echo "on"
    else
        echo "off"   
    fi     
}

function tapping_on() {
    xinput --set-prop "$TOUCHPAD_DEVICE" "libinput Tapping Enabled" 1
}

function tapping_off() {
    xinput --set-prop "$TOUCHPAD_DEVICE" "libinput Tapping Enabled" 0
}

if [ $# -eq 0 ]
then
    echo "no command specified"
else
    if [ "$1" == "tapping-check" ]
    then
        tapping_check
    elif [ "$1" == "tapping-on" ]
    then
        tapping_on
    elif [ "$1" == "tapping-off" ]
    then
        tapping_off
    fi
fi