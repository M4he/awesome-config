#!/bin/bash

# This script controls the brightness of an externally connected monitor
# via `redshift`. It uniquely identifies monitors using EDID in order to
# apply device-specific color tweaks, such as color temperature as well.
# The brightness level is to be passed as the first and only parameter as
# a float value within the range 0.1 .. 1.0

# example: external-brightness.sh 0.7

CWD=$(dirname "$0")  # working directory = path of this script

# determine external display
EXT="NONE"
xrandr | grep "^HDMI-1 connected" &> /dev/null && EXT="HDMI-1"
xrandr | grep "^HDMI-2 connected" &> /dev/null && EXT="HDMI-2"
xrandr | grep "^DP-1 connected" &> /dev/null && EXT="DP-1"
xrandr | grep "^DP-2 connected" &> /dev/null && EXT="DP-2"
echo "Detected external display $EXT"

MONITOR_INFO=$(python3 $CWD/get-crtc-edid.py "$EXT")

# split by colon character
IFS=':' read -ra MONITOR_IDS <<< "$MONITOR_INFO"
CRTC=${MONITOR_IDS[0]}
EDID=${MONITOR_IDS[1]}

BRIGTHNESS_LEVEL=$1
# custom tweaks per monitor
if [[ "$EDID" == "bd87021be72ff9de07e93ff41235a435" ]]; then
    redshift -m randr:crtc=$CRTC -PO 6400 -b $BRIGTHNESS_LEVEL:$BRIGTHNESS_LEVEL
elif [[ "$EDID" == "112139aaa00b12668c30ecd1a56ba44e" ]]; then
    redshift -m randr:crtc=$CRTC -PO 6400 -b $BRIGTHNESS_LEVEL:$BRIGTHNESS_LEVEL
else
    # unknown/untweaked device
    redshift -m randr:crtc=$CRTC -PO 6500 -b $BRIGTHNESS_LEVEL:$BRIGTHNESS_LEVEL
fi