#!/bin/bash

EXT="NONE"
xrandr | grep "^HDMI-1 connected" &> /dev/null && EXT="HDMI-1"
xrandr | grep "^HDMI-2 connected" &> /dev/null && EXT="HDMI-2"
xrandr | grep "^DP-1 connected" &> /dev/null && EXT="DP-1"
xrandr | grep "^DP-2 connected" &> /dev/null && EXT="DP-2"
echo "Detected external display $EXT"
CWD=$(dirname "$0")  # working directory = path of this script

function is_4k_display() {
    if xrandr | grep -A1 " connected" | grep -A1 "^DP-1" | tail -n1 | grep "x2160"
    then
        true
    else
        false
    fi
}

case "$1" in
    primary)
        xrandr --output eDP-1 --primary --auto --output DP-2 --off --output DP-1 --off;
        ;;
    secondary)
        if is_4k_display
        then
            xrandr --output $EXT --primary --mode 1920x1080 --output eDP-1 --off;
        else
            xrandr --output $EXT --primary --auto --output eDP-1 --off;
        fi
        bash $CWD/external-brightness.sh 1.0
        ;;
    extend)
        if is_4k_display
        then
            xrandr --output eDP1 --primary --auto --output $EXT --mode 1920x1080 --right-of eDP-1;
        else
            xrandr --output eDP1 --primary --auto --output $EXT --auto --right-of eDP-1;
        fi
        bash $CWD/external-brightness.sh 1.0
        ;;
    mirror)
        if is_4k_display
        then
            xrandr --output $EXT --primary --mode 1920x1080 --output eDP-1 --auto --same-as $EXT;
        else
            xrandr --output $EXT --primary --auto --output eDP-1 --auto --same-as $EXT;
        fi
        ;;
    ontop)
        if is_4k_display
        then
            xrandr --output $EXT --primary --mode 1920x1080 --output eDP-1 --auto --below $EXT;
        else
            xrandr --output $EXT --primary --auto --output eDP-1 --auto --below $EXT;
        fi
        bash $CWD/external-brightness.sh 1.0
        ;;
    *)
        echo "WRONG INPUT"
        ;;
esac

# parameter: abstract icon name
# output: absolute path to the resolved icon of the current Gtk theme
function retrieve_gtk_icon() {
    SIZE=48
    python3 -c "import gi; gi.require_version('Gtk', '3.0'); from gi.repository import Gtk; print(Gtk.IconTheme.get_default().lookup_icon('$1', $SIZE, 0).get_filename())"
}

# workaround for FITFORT USB-C hub: disable screen DPMS sleep to prevent display disconnects
ICON=$(retrieve_gtk_icon 'display')
if [ "$EXT" != "NONE" ]
then
    bash -c "sleep 3; lsusb | grep 05e3:0626 > /dev/null; if [ \"\$?\" == \"0\" ]; then xset dpms 0 0 0; notify-send -i '$ICON' 'DPMS disabled' 'DPMS has been disabled for this display adapter to prevent screen disconnects'; fi"
fi