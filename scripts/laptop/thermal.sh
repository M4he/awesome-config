#!/bin/bash

# add the following line to your sudoers file (run `visudo` for editing it)
# %sudo   ALL=NOPASSWD:/usr/sbin/rfkill

NON_QUIET_PROFILE="Balanced"

function quiet_check() {
    ACTIVE_PROFILE=$(sudo smbios-thermal-ctl -g | grep -A1 "Current Thermal Modes:" | tail -n1 | cut -d' ' -f2)
    if [ "$ACTIVE_PROFILE" == "Quiet" ]
    then
        echo "on"
    else
        echo "off"   
    fi     
}

function turn_quiet_on() {
    sudo smbios-thermal-ctl --set-thermal-mode=Quiet
}

function turn_quiet_off() {
    sudo smbios-thermal-ctl --set-thermal-mode=$NON_QUIET_PROFILE
}

if [ $# -eq 0 ]
then
    echo "no command specified"
else
    if [ "$1" == "quiet-check" ]
    then
        quiet_check
    elif [ "$1" == "quiet-on" ]
    then
        turn_quiet_on
    elif [ "$1" == "quiet-off" ]
    then
        turn_quiet_off
    fi
fi