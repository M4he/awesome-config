#!/bin/bash

# This script is an extension mode to rofi
#
# Use it as a separate rofi mode like this:
# `rofi ... -modi drun,run,window,EXT:/path/to/this/script.sh`
#
# or merge/combine the entries with another mode like this:
# `rofi ... -combi-modi drun,EXT:/path/to/this/script.sh`

declare -A COMMANDS
COMMANDS['Toggle: Bluetooth']='bash ../rfkill.sh bt-toggle'
COMMANDS['Toggle: WiFi']='bash ../rfkill.sh wifi'
COMMANDS['Monitor: extend to right']='bash xrandr.sh extend'
COMMANDS['Monitor: place external ontop']='bash xrandr.sh ontop'
COMMANDS['Monitor: switch to primary']='bash xrandr.sh primary'
COMMANDS['Monitor: switch to secondary']='bash xrandr.sh secondary'
COMMANDS['Monitor: mirror displays']='bash xrandr.sh mirror'
COMMANDS['Presentation mode on (SFW)']='$HOME/.styles/stylechanger.py SFW'
COMMANDS['Presentation mode off (NSFW)']='$HOME/.styles/stylechanger.py'
COMMANDS['Enable Trackpad Tapping']='bash input.sh tapping-on'
COMMANDS['Disable Trackpad Tapping']='bash input.sh tapping-off'

if [[ $# -eq 0 ]]; then
    # list all commands
    for CMD in "${!COMMANDS[@]}"; do
        echo "${CMD}"
    done
else
    # execute command
    CMD=${COMMANDS["$@"]}
    CWD=$(dirname "$0")  # working directory = path of this script
    # discard output and start as background process within bash
    # so that it doesn't block or open rofi again
    bash -c "cd \"$CWD\"; $CMD" &>/dev/null &
fi