#!/bin/bash

# HDMI audio switchting
INTERNAL_SOUND_CARD="alsa_card.pci-0000_00_1f.3"
SOUND_PROFILE_BASIC="output:analog-stereo+input:analog-stereo"
SOUND_PROFILE_HDMI1="output:hdmi-stereo"
SOUND_PROFILE_HDMI2="output:hdmi-stereo-extra1"

function hdmi_audio_check() {
    ACTIVE_PROFILE=$(LC_ALL=C pacmd list-cards | grep 'active profile.*output' | cut -d'<' -f2 | cut -d'>' -f1 | head -n1)
    if [ "$ACTIVE_PROFILE" == "$SOUND_PROFILE_HDMI1" ]
    then
        echo "on"
    elif [ "$ACTIVE_PROFILE" == "$SOUND_PROFILE_HDMI2" ]
    then
        echo "on"
    else
        echo "off"   
    fi     
}

function get_availability_for_profile () {
    AVAIL=$(LC_ALL=C pactl list cards | grep "${1}:.*available" | awk -F'available: ' '{print $2}' | cut -d')' -f1)
    echo $AVAIL
}

function hdmi_audio_turn_on() {
    SOUND_PROFILE_HDMI1_AVAIL=$(get_availability_for_profile "$SOUND_PROFILE_HDMI1")
    SOUND_PROFILE_HDMI2_AVAIL=$(get_availability_for_profile "$SOUND_PROFILE_HDMI2")
    if [ "$SOUND_PROFILE_HDMI1_AVAIL" == "yes" ]
    then
        echo "Profile $SOUND_PROFILE_HDMI1 is available and will be set"
        pactl set-card-profile "$INTERNAL_SOUND_CARD" "$SOUND_PROFILE_HDMI1"
    elif [ "$SOUND_PROFILE_HDMI2_AVAIL" == "yes" ]
    then
        echo "Profile $SOUND_PROFILE_HDMI2 is available and will be set"
        pactl set-card-profile "$INTERNAL_SOUND_CARD" "$SOUND_PROFILE_HDMI2"
    else
        echo "No profile available"
    fi
}

function hdmi_audio_turn_off() {
    pactl set-card-profile "$INTERNAL_SOUND_CARD" "$SOUND_PROFILE_BASIC"
}

if [ $# -eq 0 ]
then
    echo "no command specified"
else
    if [ "$1" == "hdmi-check" ]
    then
        hdmi_audio_check
    elif [ "$1" == "hdmi-on" ]
    then
        hdmi_audio_turn_on
    elif [ "$1" == "hdmi-off" ]
    then
        hdmi_audio_turn_off
    fi
fi