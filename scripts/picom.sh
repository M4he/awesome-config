#!/bin/bash
#

CONFIG_DIR=${XDG_CONFIG_HOME:-~/.config}

turn_on() {
    picom &
}

turn_on_experimental() {
    picom --experimental-backends &
}

turn_off() {
    killall picom
}

blur_check() {
    METHOD=$(grep -F 'method = "none";' "$CONFIG_DIR/picom.conf" | tr -d '[\"\;\=\ ]')
    if [ "$METHOD" == "methodnone" ]
    then
        echo "off"
    else
        echo "on"
    fi
}

turn_blur_on() {
    sed -i --follow-symlinks -e 's/method = "none"/method = "dual_kawase"/g' "$CONFIG_DIR/picom.conf"
}

turn_blur_off() {
    sed -i --follow-symlinks -e 's/method = "dual_kawase"/method = "none"/g' "$CONFIG_DIR/picom.conf"
}

if [ $# -eq 0 ]
then
    echo "no command specified"
else
    if [ "$1" == "check" ]
    then
        (pgrep -x "picom" > /dev/null && echo "on") || echo "off"
    elif [ "$1" == "on" ]
    then
        turn_on
    elif [ "$1" == "on-experimental" ]
    then
        turn_on_experimental
    elif [ "$1" == "off" ]
    then
        turn_off
    elif [ "$1" == "blur-check" ]
    then
        blur_check
    elif [ "$1" == "blur-on" ]
    then
        turn_blur_on
    elif [ "$1" == "blur-off" ]
    then
        turn_blur_off
    fi
fi