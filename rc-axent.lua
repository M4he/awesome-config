-----------------------------------------------------------------------------------------------------------------------
--                                                   aXent config                                                    --
-----------------------------------------------------------------------------------------------------------------------

-- Load modules
-----------------------------------------------------------------------------------------------------------------------

-- Standard awesome library
------------------------------------------------------------
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

require("awful.autofocus")

-- User modules
------------------------------------------------------------
local redflat = require("redflat")
local redflat_extra = require("redflat-extra")
redflat.startup:activate()


-- Compositor
------------------------------------------------------------
local picom_args = _G.is_laptop and "" or ""
awful.spawn.with_shell("killall picom; sleep 0.25; picom" .. picom_args)


-- Error handling
-----------------------------------------------------------------------------------------------------------------------
require("axent.ercheck-config") -- load file with error handling


-- Setup theme and environment vars
-----------------------------------------------------------------------------------------------------------------------
local env = require("axent.env-config") -- load file with environment
env:init({ theme = "axent" })

-- Screen blanking
local splashscreen = require("redflat-extra.splashscreen")
splashscreen:show()

-- Setup rofi styling
local rofi = require("axent.rofi-config")
rofi:init({ env = env })
env.rofi = rofi.cmd


-- Layouts setup
-----------------------------------------------------------------------------------------------------------------------
local layouts = require("axent.layout-config") -- load file with tile layouts setup
layouts:init()


-- Main menu configuration
-----------------------------------------------------------------------------------------------------------------------
local mymenu = require("axent.menu-config") -- load file with menu configuration
mymenu:init({ env = env })


-- Logout screen configuration
-----------------------------------------------------------------------------------------------------------------------
local logout = require("axent.logout-config")
logout:init()


-- Additional widgets
-----------------------------------------------------------------------------------------------------------------------
local widgets = require("axent.widgets-config")
local controlcenter = redflat_extra.controlcenter
controlcenter:init()

-- Panel widgets
-----------------------------------------------------------------------------------------------------------------------

-- Separator
--------------------------------------------------------------------------------
local separator = redflat.gauge.separator.vertical()
local startmenu_separator = redflat.gauge.separator.vertical(beautiful.panel.start.separator or {})

-- Tasklist
--------------------------------------------------------------------------------
local tasklist = {}

-- load list of app name aliases from files and set it as part of tasklist theme
tasklist.style = { appnames = require("axent.alias-config")}

tasklist.buttons = awful.util.table.join(
	awful.button({}, 1, redflat.widget.tasklist.action.select),
	awful.button({}, 2, redflat.widget.tasklist.action.close),
	awful.button({}, 3, redflat.widget.tasklist.action.menu),
	awful.button({}, 4, redflat.widget.tasklist.action.switch_next),
	awful.button({}, 5, redflat.widget.tasklist.action.switch_prev)
)

-- Taglist widget
--------------------------------------------------------------------------------
local taglist = {}
taglist.style = { separator = separator, widget = redflat.gauge.tag.orange.new }
taglist.buttons = awful.util.table.join(
	awful.button({         }, 1, function(t) t:view_only() end),
	awful.button({ env.mod }, 1, function(t) if client.focus then client.focus:move_to_tag(t) end end),
	awful.button({         }, 2, awful.tag.viewtoggle),
	awful.button({         }, 3, function(t) redflat.widget.layoutbox:toggle_menu(t) end),
	awful.button({ env.mod }, 3, function(t) if client.focus then client.focus:toggle_tag(t) end end),
	awful.button({         }, 4, function(t) awful.tag.viewnext(t.screen) end),
	awful.button({         }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

-- Textclock widget
--------------------------------------------------------------------------------
local textclock = {}
textclock.widget = redflat.widget.textclock({ timeformat = "%d. %b %H:%M" })

redflat.float.calendar:init()
textclock.buttons = awful.util.table.join(
	awful.button({}, 1, function() redflat.float.calendar:show() end)
)

-- Layoutbox configure
--------------------------------------------------------------------------------
local layoutbox = {}

layoutbox.buttons = awful.util.table.join(
	awful.button({ }, 1, function () redflat.widget.layoutbox:toggle_menu(mouse.screen.selected_tag) end),
	awful.button({ }, 3, function () redflat.widget.layoutbox:toggle_menu(mouse.screen.selected_tag) end)
)

-- Tray widget
--------------------------------------------------------------------------------
local tray = {}
tray.widget = redflat.gauge.svgbox(
	beautiful.icon.widget.controls, nil, beautiful.color.gray
)

tray.buttons = awful.util.table.join(
	awful.button({}, 1, function() controlcenter:toggle() end)
)

-- PA volume control
--------------------------------------------------------------------------------
local volume = {}
volume.widget = redflat.widget.pulse({ timeout = 3, autoupdate = true }, { widget = redflat.gauge.audio.blue.new })

-- right bottom corner position
local rb_corner = function()
	return { x = screen[mouse.screen].workarea.x + screen[mouse.screen].workarea.width,
			 y = screen[mouse.screen].workarea.y + screen[mouse.screen].workarea.height }
end

-- activate player widget
redflat.float.player:init({ name = env.player })

volume.buttons = awful.util.table.join(
	awful.button({}, 4, function() volume.widget:change_volume()                end),
	awful.button({}, 5, function() volume.widget:change_volume({ down = true }) end),
	awful.button({}, 2, function() volume.widget:mute()                         end),
	awful.button({}, 3, function() redflat.float.player:show(rb_corner())       end),
	awful.button({}, 1, function() redflat.float.player:action("PlayPause")     end),
	awful.button({}, 8, function() redflat.float.player:action("Previous")      end),
	awful.button({}, 9, function() redflat.float.player:action("Next")          end)
)


-- System resource monitoring widgets
--------------------------------------------------------------------------------
local sysmon = { widget = {}, buttons = {}, icon = {} }

-- icons
sysmon.icon.network = redflat.util.table.check(beautiful, "icon.widget.wireless")
sysmon.icon.cpuram = redflat.util.table.check(beautiful, "icon.widget.monitor")
if _G.is_laptop then
	sysmon.icon.battery = redflat.util.table.check(beautiful, "icon.widget.battery")
	sysmon.icon.sensor = redflat.util.table.check(beautiful, "icon.widget.sensor")

	-- Battery
	sysmon.widget.battery = redflat.widget.sysmon(
		{ func = redflat.system.pformatted.bat(25), arg = "BAT0" },
		{
			timeout = 60,
			widget = redflat.gauge.icon.single,
			monitor = {
				is_vertical = true,
				icon = sysmon.icon.battery,
				color = beautiful.gauge.icon.single.gray.color
			},
		}
	)

	-- Thermals
	sysmon.widget.sensor = redflat.widget.sysmon(
		{ func = redflat_extra.system.pformatted.sensors(90, 105) },
		{
			timeout = 3,
			widget = redflat.gauge.icon.single,
			monitor = {
				is_vertical = true,
				icon = sysmon.icon.sensor,
				color = beautiful.gauge.icon.single.gray.color
			}
		}
	)
end

-- CPU usage
-- we need to store this callback outside of the widget definition because it
-- internally defines storage tables which need to be persistent across calls
-- which will not work if the '.cpu()' call is inside the 'func' definition of
-- the sysmon widget!
local cpu_func = redflat.system.pformatted.cpu(80)
sysmon.widget.cpu = redflat.widget.sysmon(
	{ func = function()
		local info = cpu_func()
		info.text = info.text .. " CPU"
		return info
	end },
	{ timeout = 2, monitor = { label = "CPU" } }
)

sysmon.buttons.cpu = awful.util.table.join(
	awful.button({ }, 1, function() redflat.float.top:show("cpu") end),
	awful.button({ }, 3, function() awful.spawn(env.sysmon) end)
)

-- RAM usage
sysmon.widget.ram = redflat.widget.sysmon(
	{ func = function()
		local mem = redflat.system.memory_info()
		local info = redflat.system.pformatted.mem(80)()
		info.text = info.text .. " RAM | " .. mem.swp.usep .. "% swap"
		return info
	end },
	{ timeout = 10, monitor = { label = "RAM" } }
)

sysmon.buttons.ram = awful.util.table.join(
	awful.button({ }, 1, function() redflat.float.top:show("mem") end),
	awful.button({ }, 3, function() awful.spawn(env.sysmon) end)
)

-- Main menu button (start button)
local startmenu = { widget = {}, buttons = {}, deco = {} }
startmenu.widget = redflat.gauge.svgbox(
	beautiful.icon.awesome, nil, beautiful.panel.start.color.icon or beautiful.color.icon
)
startmenu.buttons = awful.util.table.join(
	awful.button({ }, 1, function ()
		local wa = mouse.screen.workarea
		mymenu.mainmenu:toggle({ coords = { x = wa.x, y = wa.y + wa.height } })
	end),
	awful.button({ }, 3, function ()
		local wa = mouse.screen.workarea
		mymenu.sysmenu:toggle({ coords = { x = wa.x, y = wa.y + wa.height } })
	end)
)
if beautiful.start_button_decorator_enabled then
	startmenu.deco = widgets.start_button_decorator()
	startmenu.deco:buttons(startmenu.buttons)
end


-- Wallpaper setup
-----------------------------------------------------------------------------------------------------------------------

-- redraw the wallpaper(s) on the root window according to the current layout
local function draw_wallpaper()
	awful.spawn.with_shell("nitrogen --restore --force-setter=xinerama")
end

-- draw wallpaper initially after startup
draw_wallpaper()

-- redraw wallpaper if screen layout/size changes
screen.connect_signal("property::geometry", function()
	draw_wallpaper()
end)


-- Screen setup
-----------------------------------------------------------------------------------------------------------------------

-- panel setup
local wibar_ontop = true

-- setup
local wibar_bg = beautiful.panel.color.bg or beautiful.color.wibox

awful.screen.connect_for_each_screen(
	function(s)
		-- wallpaper (DEPRECATED in favor of nitrogen)
		-- env.wallpaper(s)

		-- tags
		if screen.primary.index == s.index then
			-- PRIMARY SCREEN
			awful.tag({ "Main", "Com", "Code", "Tile", "Free" }, s,
				{
					layouts.float,
					layouts.max,
					layouts.tile,
					layouts.tile,
					layouts.float
				}
			)

			-- layoutbox widget
			layoutbox[s] = redflat.widget.layoutbox({ screen = s })

			-- taglist widget
			taglist[s] = redflat.widget.taglist({ screen = s, buttons = taglist.buttons, hint = widgets.tagtip }, taglist.style)

			-- tasklist widget
			tasklist[s] = redflat.widget.tasklist({ screen = s, buttons = tasklist.buttons }, tasklist.style)

			-- panel wibox
			s.panel = awful.wibar({
				position = "bottom",
				ontop = wibar_ontop,
				screen = s,
				height = beautiful.panel.height or 36,
				bg = wibar_bg,
			})
			s.panel_border = widgets.panel_border(s)

			-- add widgets to the wibox
			s.panel:setup {
				layout = wibox.layout.align.horizontal,
				{ -- left widgets
					layout = wibox.layout.fixed.horizontal,

					widgets.wrapper(startmenu.widget, "startmenu", startmenu.buttons),
					beautiful.start_button_decorator_enabled and startmenu.deco or startmenu_separator,
					widgets.wrapper(taglist[s], "taglist"),
					separator,
					widgets.wrapper(layoutbox[s], "layoutbox", layoutbox.buttons),
				},
				{ -- middle widget
					layout = wibox.layout.align.horizontal,
					expand = "outside",
					nil,
					widgets.wrapper(tasklist[s], "tasklist"),
				},
				{ -- right widgets
					layout = wibox.layout.fixed.horizontal,

					separator,
					widgets.wrapper(volume.widget, "volume", volume.buttons),
					separator,
					widgets.wrapper(sysmon.widget.cpu, "cpu", sysmon.buttons.cpu),
					_G.is_laptop and widgets.wrapper(sysmon.widget.sensor, "sensor") or nil,
					separator,
					widgets.wrapper(sysmon.widget.ram, "ram", sysmon.buttons.ram),
					separator,
					widgets.wrapper(tray.widget, "tray", tray.buttons),
					separator,
					_G.is_laptop and widgets.wrapper(sysmon.widget.battery, "battery") or nil,
					_G.is_laptop and separator or nil,
					widgets.wrapper(textclock.widget, "textclock", textclock.buttons),
				},
			}

		else
			-- ANY NON-PRIMARY SCREEN

			if _G.is_laptop then
				awful.tag({ "Main", "Sub" }, s,
					{
						layouts.tile,
						layouts.float
					}
				)
			else
				awful.tag({ "Main" }, s,
					{
						layouts.center
					}
				)
			end

			-- layoutbox widget
			layoutbox[s] = redflat.widget.layoutbox({ screen = s })

			if _G.is_laptop then
				-- taglist widget
				taglist[s] = redflat.widget.taglist({ screen = s, buttons = taglist.buttons, hint = widgets.tagtip }, taglist.style)
			end

			-- tasklist widget
			tasklist[s] = redflat.widget.tasklist({ screen = s, buttons = tasklist.buttons }, tasklist.style)

			s.panel = awful.wibar({
				position = "bottom",
				ontop = wibar_ontop,
				screen = s,
				height = beautiful.panel_height or 36,
				bg = wibar_bg
			})
			s.panel_border = widgets.panel_border(s)

			-- add widgets to the wibox
			s.panel:setup {
				layout = wibox.layout.align.horizontal,
				{ -- left widgets
					layout = wibox.layout.fixed.horizontal,

					widgets.wrapper(startmenu.widget, "startmenu", startmenu.buttons),
					beautiful.start_button_decorator_enabled and startmenu.deco or startmenu_separator,
					_G.is_laptop and widgets.wrapper(taglist[s], "taglist") or nil,
					_G.is_laptop and separator or nil,
					widgets.wrapper(layoutbox[s], "layoutbox", layoutbox.buttons),
				},
				{ -- middle widget
					layout = wibox.layout.align.horizontal,
					expand = "outside",
					nil,
					widgets.wrapper(tasklist[s], "tasklist"),
				},
				{ -- right widgets
					layout = wibox.layout.fixed.horizontal,
					separator,
					widgets.wrapper(tray.widget, "tray", tray.buttons),
					separator,
					widgets.wrapper(textclock.widget, "textclock", textclock.buttons),
				},
			}

		end

	end
)

-- ONTOP WIBOX WORKAROUND
-- temporary abandons the panel's ontop property for fullscreen windows
if wibar_ontop then

	local wibox_ontop_check = function(s)
		local wibox_ontop = true
		-- check for fullscreen layout
		if awful.layout.get(screen[s]).name == "fullscreen" then
			wibox_ontop = false
		else
		-- check for any fullscreen client
			for _, c in pairs(awful.client.visible(s)) do
				if c.fullscreen then
					wibox_ontop = false
					break
				end
			end
		end
		screen[s].panel.ontop = wibox_ontop
		screen[s].panel_border.wibox.ontop = wibox_ontop and beautiful.panel.border_ontop
	end

	for s=1, screen.count() do
		screen[s]:connect_signal("arrange", function()
			wibox_ontop_check(s)
		end)
	end
	client.connect_signal("property::fullscreen", function(c)
		wibox_ontop_check(c.screen)
	end)

end


-- Active screen edges
-----------------------------------------------------------------------------------------------------------------------
local edges = require("axent.edges-config") -- load file with edges configuration
edges:init()


-- Wallpaper blacking for max layouts
-----------------------------------------------------------------------------------------------------------------------
local blackout = require("axent.blackout-config")
blackout:init()


-- Key bindings
-----------------------------------------------------------------------------------------------------------------------
local hotkeys = require(_G.is_laptop and "axent.keys-config-laptop" or "axent.keys-config") -- load file with hotkeys configuration
hotkeys:init({ env = env, menu = mymenu.mainmenu, volume = volume.widget })


-- Titlebar setup
-----------------------------------------------------------------------------------------------------------------------
local titlebar = require("axent.titlebar-config") -- load file with titlebar configuration
titlebar:init()


-- Rules
-----------------------------------------------------------------------------------------------------------------------
-- NOTE: titlebar:init() needs to precede this, because it might adjust beautiful.border_width!
local rules = require("axent.rules-config") -- load file with rules configuration
rules:init({ hotkeys = hotkeys})


-- Base signal set for awesome wm
-----------------------------------------------------------------------------------------------------------------------
local signals = require("axent.signals-config") -- load file with signals configuration
signals:init({ env = env })


-- Battery charge listener
-----------------------------------------------------------------------------------------------------------------------
local signals = require("axent.battery-config") -- load file with signals configuration
signals:init({ low = 20, high = 100 })


-- Autostart user applications
-----------------------------------------------------------------------------------------------------------------------
local autostart = require("axent.autostart-config") -- load file with autostart application list

if redflat.startup.is_startup then
	local auto_file = awful.util.get_configuration_dir() .. (_G.is_laptop and "axent/etc/autostart.laptop" or "axent/etc/autostart.main")
	autostart.run_from_file(auto_file)
	autostart.run()
end

-- refresh screen locking and screensaver timeouts
if _G.is_laptop then
	awful.spawn.with_shell("xset dpms 600 0 0; xset s 120 0")
else
	awful.spawn.with_shell("xset dpms 0 0 0; xset s 0 0")
end

-- we disable aero snap for now
awful.mouse.snap.edge_enabled = false

-- disable the annoying 'busy' mouse cursor when awful.spawn* is used
beautiful.enable_spawn_cursor = false
